function [  ] = perfectGraphGen( p )

primeNumbers = primes(p(end));
largestNumber = primeNumbers(end);
perfectArrayBool = zeros(1,largestNumber);
primeNumbersArray = zeros(1,largestNumber);
x = 1:largestNumber;

for j = 1:numel(p)
    perfectArrayBool(p(j)) = 1; 
end

for j = 1:numel(primeNumbers)
    primeNumbersArray(primeNumbers(j)) = 2; 
end


plot(x,perfectArrayBool,'r')
hold;
plot(x,primeNumbersArray,'b');




