%Get velocities
zValuesVelocity = zeros([1,2000]);
n = 0;
v0 = 0; 

for m = 1:1999,
    
    b = m+1;
    a = m;
    
   zValuesVelocity(m) = ((b-a)/6) * ( zValuesAccel(a) + (4*((zValuesAccel(a) + zValuesAccel(b))/2 )) + zValuesAccel(b)) + v0;    
   v0 = zValuesVelocity(m);
   
end

%Get acceleration
zValuesDisplace = zeros([1,2000]);
n = 0;
z0 = 0; 

for m = 1:1999,
    
    b = m+1;
    a = m;
    
   zValuesDisplace(m) = ((b-a)/6) * ( zValuesVelocity(a) + (4*((zValuesVelocity(a) + zValuesVelocity(b))/2 )) + zValuesVelocity(b)) + z0;    
   z0 = zValuesDisplace(m);
   
end