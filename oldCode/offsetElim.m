function [ biasless ] = offsetElim( data )
%Get rid of offset. 

%Current offset will be the difference between the mean and 0
currentOffset = 0;
sensitivity = 0.001;

for j=1:10:(numel(data)-10)
    
    for h = 1:10
        
    currentBox(h) = data(h + j);
    
    end
    
    
    if range(currentBox) < sensitivity
        
        currentOffset = mean(currentBox);
        
        for k = j:(j+9)
        
        data(k) = 0;
            
        end
    
    else
        
        for a = j:(j+9)
        
        data(a) = data(a) - currentOffset;
            
        end
        
        
        end
        
end
 
biasless = data;

end

