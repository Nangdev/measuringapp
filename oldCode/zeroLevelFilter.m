%Current offset will be the difference between the mean and 0
currentOffset = 0;
sensitivity = 0.3;

for j=1:10:(numel(yValuesAccel)-10)
    
    for h = 1:10
        
    currentBox(h) = yValuesAccel(h + j);
    
    end
    
    
    if range(currentBox) < sensitivity
        
        currentOffset = mean(currentBox);
        
        for k = j:(j+9)
        
        yValuesAccel(k) = 0;
            
        end
    
    else
        
        for a = j:(j+9)
        
        yValuesAccel(a) = yValuesAccel(a) - currentOffset;
            
        end
        
        
        end
        
end
    