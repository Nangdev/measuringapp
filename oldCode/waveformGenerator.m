Fs = 200000;                    % Sampling frequency
T = 1/Fs;                     % Sample time
L = 200000;                     % Length of signal
t = (0:L-1)*T;                % Time vector
% Sum of a 50 Hz sinusoid and a 120 Hz sinusoid
x = 4.3*sin(2*pi*2.241*t) + 4.2*sin(2*pi*2.561*t); 
%waveForm = x + 2*randn(size(t));     % Sinusoids plus noise
figure(3);
plot(x(1:L))