function [ endSample ] = endOfSignal( signal )
%endOfSignal Find the end of the signal

endCounter = 0;
endSample = 0;

for j=1,numel(signal)
    
   if signal(j) == 0 
       endCounter = endCounter + 1;
   else
       endCounter= 0;
   end
   
   if endCounter == 30
       endSample = j - 35 ;
       break;
   end
   
    if j == numel(signal)
        endSample = numel(signal) - 25;
        break;
    end
end

end

