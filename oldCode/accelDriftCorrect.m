function [ integrand ] = accelDriftCorrect( integrand, sensitivity )
%accelDriftCorrect Corrects for acceleromter drift


%Current offset will be the difference between the mean and 0
currentOffset = 0;

for j=1:10:(numel(integrand)-10)
    
    for h = 1:10
        
    currentBox(h) = integrand(h + j);
    
    end
    
    
    if range(currentBox) < sensitivity
        
        currentOffset = mean(currentBox);
        
        for k = j:(j+9)
        
        integrand(k) = 0;
            
        end
    
    else
        
        for a = j:(j+9)
        
        integrand(a) = integrand(a) - currentOffset;
            
        end
        
        
        end
        
end
    


end

