y = yValuesAccel(1:noOfElements);
Fs = sampleFreq;
shiftFactor = 1;
L = noOfElements + shiftFactor;


NFFT = 2^nextpow2(L); % Next power of 2 from length of y
Y = fft(y,NFFT)/L;

%Shift freq domain signal 
% shiftedArray = zeros(1, (shiftFactor + numel(Y)));
% shiftedArray(1,(shiftFactor+1):end) = Y;
% Y = shiftedArray;
% plot(Y);

f = Fs/2*linspace(0,1,NFFT/2+1);
yaxis = 2*abs(Y(1:NFFT/2+1));

% Plot single-sided amplitude spectrum.
figure(1)
plot(f,yaxis) 
title('Single-Sided Amplitude Spectrum of y(t)')
xlabel('Frequency (Hz)')
ylabel('|Y(f)|')

% Plot the inverse fft
iY = ifft(Y)*300;
figure(2);
plot(iY) 
title('Original time domain signal')
xlabel('Time')
ylabel('Acceleration')
