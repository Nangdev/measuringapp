function [ ] = masterFunction( rawTimeData, rawYValues, timeStep )
%The controlling function. 

%Configuration
cutoffFreq = 5; %For butterworth filter
sampleRate = 1000;

%timeSeries will come out trimmed from this step
[timeSeries, length] = seriesFromStamp(rawTimeData);

trimmedY = trimZeroData(rawYValues);

interpY = interpSeries(timeSeries, trimmedY, timeStep);

%i.e. had 0hz component zeroed
fftY = interpfft( interpY, timeStep );

sig = interpButter( fftY, cutoffFreq, sampleRate);
figure;
plot(sig);

sig = offsetElim(sig);
figure;
plot(sig);

end

