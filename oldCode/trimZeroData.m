function [ trimedArray, lengthCounter ] = trimZeroData( rawData )
%Trims the input data of zero bins at the end
%Also gives back the length of the data

lengthCounter = 0;
    
for j = 1:numel(rawData)
   
if(rawData(j) == 0)
   
    lengthCounter = j - 1;
    break;
    
end

end

trimedArray = rawData(1:lengthCounter);

end



