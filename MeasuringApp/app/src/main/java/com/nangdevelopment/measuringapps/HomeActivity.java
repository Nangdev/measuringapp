package com.nangdevelopment.measuringapps;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Date;

public class HomeActivity extends Activity implements SensorEventListener{

    private SensorManager mSensorManager;
    private Sensor mAccel;
    private Sensor mGravity;
    private Sensor mGyro;
    private String outputString = "";
    private Boolean record = false;
    Button measureIt;

    private float[][] accelX = new float[500][500];
    private float[][] accelY = new float[500][500];
    private float[][] accelZ = new float[500][500];
    private float[][] gyroX = new float[500][500];
    private float[][] gyroY = new float[500][500];
    private float[][] gyroZ = new float[500][500];
    int accelIndex = 0;
    int gyroIndex = 0;
    long timeStamp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        measureIt = (Button) findViewById(R.id.measure_button);

        measureIt.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Toast.makeText(getApplicationContext(), "Recording started", Toast.LENGTH_SHORT).show();
                        record = true;
                        break;
                    case MotionEvent.ACTION_UP:
                        Toast.makeText(getApplicationContext(), "Recording stopped", Toast.LENGTH_SHORT).show();
                        record = false;
                        arrayToCsv(accelX, "accelX");
                        arrayToCsv(accelY, "accelY");
                        arrayToCsv(accelZ, "accelZ");
                        arrayToCsv(gyroX, "gyroX");
                        arrayToCsv(gyroY, "gyroY");
                        arrayToCsv(gyroZ, "gyroZ");
                        accelIndex = 0;
                        gyroIndex = 0;
                        accelX = new float[500][500];
                        accelY = new float[500][500];
                        accelZ = new float[500][500];
                        gyroX = new float[500][500];
                        gyroY = new float[500][500];
                        gyroZ = new float[500][500];
//                        writeToFile(outputString);
                        break;
                }
                return true;
            }
        });

        mAccel = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mGravity = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        mGyro = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
    }

    public void onResume(){
        super.onResume();

        mSensorManager.registerListener(this, mAccel, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mGyro, SensorManager.SENSOR_DELAY_FASTEST);
    }


    public void onSensorChanged(SensorEvent sensorEvent) {

        if(record == true){
            timeStamp = sensorEvent.timestamp;

            switch (sensorEvent.sensor.getType()) {

                case Sensor.TYPE_ACCELEROMETER:
                    accelX[accelIndex][0] = timeStamp;
                    accelX[accelIndex][1] = sensorEvent.values[0];
                    accelY[accelIndex][0] = timeStamp;
                    accelY[accelIndex][1] = sensorEvent.values[1];
                    accelZ[accelIndex][0] = timeStamp;
                    accelZ[accelIndex][1] = sensorEvent.values[2];
                    accelIndex++;
                    break;
                case Sensor.TYPE_GYROSCOPE:
                    gyroX[gyroIndex][0] = timeStamp;
                    gyroX[gyroIndex][1] = sensorEvent.values[0];
                    gyroY[gyroIndex][0] = timeStamp;
                    gyroY[gyroIndex][1] = sensorEvent.values[1];
                    gyroZ[gyroIndex][0] = timeStamp;
                    gyroZ[gyroIndex][1] = sensorEvent.values[2];
                    gyroIndex++;
                    break;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public String dataLineToString(float[] dataLine){
        String tempString = "";
        for(int i = 0; i < dataLine.length; i++){
            tempString = tempString + Float.toString(dataLine[i]) + ", ";
        }
        tempString += "\n";
        return tempString;
    }

    public void arrayToCsv(float[][] inputArray, String filename){
        String tempString = "";
        for(int i = 0; i < inputArray.length; i++){
           tempString +=  Float.toString(inputArray[i][0]) + ", " + Float.toString(inputArray[i][1]) + ",\n";
        }
        Log.v("arrayToCsv", tempString);
        writeToFile(tempString, filename);
    }

    public void writeToFile(String contents, String filename) {

        //Log.v("external directory", Environment.getExternalStorageDirectory().toString());
        Log.v("Final output", contents);

        File sdCard = Environment.getExternalStorageDirectory();
        File directory = new File (sdCard.getAbsolutePath() + "/MeasuringApp/" +  new Date().toString());
        directory.mkdirs();
        File file = new File(directory, filename + ".txt");

        try {
            FileOutputStream fOut = new FileOutputStream(file);
            OutputStreamWriter osw = new OutputStreamWriter(fOut);
            osw.write(contents);
            osw.flush();
            osw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
