function [timeSeries , lengthCounter ] = seriesFromStamp( rawTimeStamp )
%seriesFromStamp Takes the time stamps and returns a time series starting
%at t = 0.

currentValue = 1;
lengthCounter = 0;

[~,lengthCounter] = trimZeroData(rawTimeStamp);

timeSeries = zeros(1, lengthCounter);
firstStamp = rawTimeStamp(1);

for j = 1:lengthCounter
   
   timeSeries(j) = rawTimeStamp(j) - firstStamp; 
    
end

timeSeries = timeSeries * 1.0e-09;
timeSeries;

end

