function [ T ] = avgTimeStampFun( timeArray )
%abgTimeStamp Calculate the average time step
%   Detailed explanation goes here

endOfMeasurement = 0;

for j=2:numel(timeArray)
   
    if timeArray(j) == 0
        endOfMeasurement = j - 1;
        endOfMeasurement
        break;
    end
    
end

y = diff(timeArray(1:endOfMeasurement));
T = mean(y)*(10^-9);

end

