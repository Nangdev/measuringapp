function [ cspectrum ] = plot_fft( data, sample_rate )
figure;
N=length(data);
dF=sample_rate/N;
f=0:dF:sample_rate-dF;
cspectrum = fft(data);
spectrum=abs(cspectrum);
plot(f(1:round(N/2)),spectrum(1:round(N/2)));
title('Single-sided spectrum');

end

