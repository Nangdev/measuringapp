function [idealizedGraph] = fourierConstructedGraph( sampledAcceleration , rawTime, Fs )
%fourierConstructedGraph Reconstruct a continuous version of the sampled
%data

Fs
%N = numel(sampledAcceleration);
[timeSeries,N] = seriesFromStamp(rawTime); 
CLSSA(N)
 
fastlomb(sampledAcceleration(1:N),timeSeries,1);

%N=400;
%x = fft(sampledAcceleration);
%x(1) = 0;
%x = ifft(x);

%figure;
%plot(x);

%x = fft(x);
% 
% 
%  dF = Fs/N;
%  f = 0:dF:Fs-dF;
%  bins = ceil(N/2)
%  fourierCoeffs = sym(zeros(2,bins));
% 
% fourierCoeffs(1,1) = abs((x(1)*2)/(N));
% 
%  figure;
%  plot(abs(x));
%  
%  for j = 2 : 3
%       
%    x(j) = x(j) * f(j);
%       
%  end

 %figure;
 %plot(ifft(x))

% figure;
% x(1) = 0;
% accel = accelDriftCorrect(ifft(x(1:N)), 0.1); 
% plot(accel);
% 
% velocity = accelDriftCorrect(cumtrapz(accel), 0.1);
% figure;
% plot(velocity);

% %a_ns
% for j = 2 : bins
%     fourierCoeffs(1,j) = -((1*2)/(N))*imag(x(j));
% end
% 
% %b_ns
% for j = 2 : bins
%     fourierCoeffs(2,j) = ((1*2)/(N))*real(x(j));
% end
% 
% syms t;
% %x = fourierCoeffs(1,1)
% x = 0
% %componentArray = sym(zeros(N/2));

% for j = 2 : bins
% %x = x + fourierCoeffs(1,j)*sin(2*pi*(j-1)*dF*t)*((j-1)*dF) + fourierCoeffs(2,j)*cos(2*pi*(j-1)*dF*t)*((j-1)*dF);
% x = x + fourierCoeffs(1,j)*sin(2*pi*(j-1)*dF*t) + fourierCoeffs(2,j)*cos(2*pi*(j-1)*dF*t);
% %componentArray(j) = fourierCoeffs(1,j)*sin(2*pi*(j-1)*dF*t) + fourierCoeffs(2,j)*cos(2*pi*(j-1)*dF*t);
% end

% %generate x from array, let's see what we get
% for j = 2 : bins
%     
%   %  x = x + componentArray(j);
%     
% end
% 
% %integrate components individually 
% for j = 2 : bins
%     
%    % componentArray(j) = int(componentArray(j));
%     
% end

%  figure;
%  velocity = int(x);
%  ezplot(velocity);
 
% 
% figure;
% velocity = int(x);
% ezplot(velocity);

% velocity = int(x);
% figure;
% ezplot(velocity);
% 
% displacement = int(velocity);
% figure;
% ezplot(displacement);

end

