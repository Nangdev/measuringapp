function [ filteredSig ] = interpButter( sig, cutoffFreq, sampleRate )
%filter interpolated signal with butterworth 

Wn = cutoffFreq/(sampleRate/2);
N = 2;
[B,A] = butter(N , Wn , 'low');
subplot(211), plot(sig);
filteredSig = filter(B,A,sig);
subplot(212),plot(filteredSig);

end

