xValuesDisplace = zeros([1,2000]);
n = 0;
x0 = 0; 

for m = 1:1999,
    
    b = m+1;
    a = m;
    
   xValuesDisplace(m) = ((b-a)/6) * ( xValuesVelocity(a) + (4*((xValuesVelocity(a) + xValuesVelocity(b))/2 )) + xValuesVelocity(b)) + x0;    
   x0 = xValuesDisplace(m);
   
end