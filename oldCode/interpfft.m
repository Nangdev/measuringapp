function [ ispectrum ] = interpfft( interpY , timeStep )
%Get the fft

sampleRate = 1/timeStep;
spectrum = plot_fft(interpY, sampleRate);
spectrum(1) = 0;
ispectrum = ifft(spectrum)

end

