%Current offset will be the difference between the mean and 0
currentOffset = 0;
boxSize = 10;

for j=1:boxSize:(numel(velocity)-boxSize)
    
    for h = 1:boxSize
        
    currentBox(h) = velocity(h + j);
    
    end
    
    
    if range(currentBox) < 0.01
        
        currentOffset = mean(currentBox);
        
        for k = j:(j+(boxSize-1))
        
        velocity(k) = 0;
            
        end
    
    else
        
        for a = j:(j+(boxSize - 1))
        
        velocity(a) = velocity(a) - currentOffset;
            
        end
        
        
        end
        
end
    