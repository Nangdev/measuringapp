function [ interpSeries ] = interpSeries( timeSeries, trimmedY, timeStep )
%Returns spline interpolated evenly sampled series.

interpSeries = interp1(timeSeries, trimmedY, 0:timeStep:timeSeries(end), 'cubic');

end

