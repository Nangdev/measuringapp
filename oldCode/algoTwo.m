function [] = algoTwo( integrand , timeArray)
%UNTITLED8 Second algorithm for single axis measurement
%  Incorporating fourier analysis methods

E = endOfSignal(integrand);
iintegrand = integrand;
T = avgTimeStampFun(timeArray);
sampleFreq = 1/T

figure(1)
plot(timeArray) 
title('Check for time array anomalies')
xlabel('Sample points')
ylabel('Time in nanoSec')

figure(2)
plot(iintegrand) 
title('Raw Data')
xlabel('Sample points')
ylabel('Acceleration')

iintegrand = accelDriftCorrect(iintegrand, 0.15);

figure(3)
plot(iintegrand) 
title('Corrected for accelerometer drift')
xlabel('Sample points')
ylabel('Acceleration')

modelAcceleration = fourierConstructedGraph(iintegrand, sampleFreq);

velocity = cumtrapz(modelAcceleration/sampleFreq);

figure(4)
plot(velocity) 
title('Velocity')
xlabel('Sample points (Values have been divided by sampleFreq')
ylabel('Velocity')





end

